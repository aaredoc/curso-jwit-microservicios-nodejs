const {personalData, preferencesColor} = require('../Controllers')
function servicio({info, color}){
    const personalDataI = personalData({info});
    const preferencesColorI = (info.edad > 18) ? preferencesColor({color}) : {color};
    return {personalDataI, preferencesColorI};
    
}

module.exports = {servicio};