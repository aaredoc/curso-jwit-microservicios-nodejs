const {servicio} = require('../Services');
function adaptador({info, color}){
    //El adaptador solo puede tener un service
    const result = servicio({info, color})
    return result
}

module.exports = {adaptador};