function personalData({info}){
    info.editor === 'vscode' && console.log("The editor owner of Microsoft");
    info.editor === 'androidstudio' && console.log("The editor owner of Google");
    info.editor === 'atom' && console.log("The editor owner of public");
    return info;
}

function preferencesColor({color}){
    colorR = (color === 'rojo') ? 'azul' :
             (color === 'blanco') ? 'negro' : 
             (color === 'azul') ? 'rojo' : color;
    return colorR;
}

module.exports = {personalData, preferencesColor};