const {findUser, existUser} = require('../Controllers')
async function servicio({id}){   
    try {
        const existUserResponse = await existUser({id});
        if(existUserResponse.statusCode !== 200){
            throw(existUserResponse.message);
        }
        if(!existUserResponse.data){
            throw("No existe el usuario");
        }
        const findUserResponse =  await findUser({id});
        if(findUserResponse.statusCode !== 200){
            throw(findUserResponse.message);
        }

        if(findUserResponse.data.info.edad < 18){
            console.log("Eres menor de edad");
        }
        return {statusCode: 200, data: findUserResponse.data};
        
    } catch (error) {
        console.log({step: 'service: Servicio', error:error.toString()});
        return {statusCode: 500, message: error.toString()};
    }
}



module.exports = {servicio};