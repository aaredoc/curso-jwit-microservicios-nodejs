const {servicio} = require('../Services');
async function adaptador({id}){
    //El adaptador solo puede tener un service
    try {
        let {statusCode, data, message} = await servicio({id})
        return {statusCode, data, message};
    } catch (error) {
        console.log({step: 'adapter: Adaptador', error:error.toString()});
        return {statusCode: 500, message: error.toString()};
    }
    
}

module.exports = {adaptador};