const {adaptador} = require('./src/Adapters');
const main = async () => {
    try {
        const result = await adaptador({id:2});
        
        if(result.statusCode !== 200) {throw(result.message)}
        console.log("Data: ", result.data) 
    } catch (error) {
        console.log(error);
    }
};

main()

//24 min